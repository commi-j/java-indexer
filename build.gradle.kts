//
group = "tk.labyrinth"
version = "0.1.0-SNAPSHOT"
//
plugins {
	//
	// https://github.com/fkorotkov/gradle-libraries-plugin
	//	id("com.github.fkorotkov.libraries").version("1.1")
	//
	`java-library`
}
//
allprojects {
	apply {
		`java-library`
	}
	//
	repositories {
		mavenCentral()
		mavenLocal()
	}
	//
	dependencies {
		//
	}
	//
	tasks {
		val versionJava = "11"
		//
		compileJava {
			sourceCompatibility = versionJava
			targetCompatibility = versionJava
		}
		//
		test {
			useJUnitPlatform()
		}
	}
}
//
dependencies {
	val versionAutoService = "1.0-rc6"
	//
	compileOnly("com.google.auto.service", "auto-service", versionAutoService)
	annotationProcessor("com.google.auto.service", "auto-service", versionAutoService)
}
//
// Utility
inline val ObjectConfigurationAction.`java-library`: ObjectConfigurationAction
	get() = plugin("java-library")
