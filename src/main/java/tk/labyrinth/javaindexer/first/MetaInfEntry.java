package tk.labyrinth.javaindexer.first;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.SOURCE)
public @interface MetaInfEntry {

	Class<?>[] indexTypes() default {};

	/**
	 * path
	 *
	 * @return wat
	 */
	String value();
}
